12.03.2020

Read this carefully, I don't have a helpdesk for support.

This is a Void Linux (x86_64, with musl!) template for building Palemoon browser.

I put it as is, without any warrants. I added jackd support in sound, because I don't use PA. Add the needed options in template, if you wish.
It was tested for x86_64-musl architecture. If you need other arch, you may try to make the appropriate changes in template.
If you need a build for glibc - remove musl patches from the appropriate folder.

I didn't remove unneeded lines in the template, I just commented them out.

I made it from Firefox-ESR and I'm too lazy for setting up all installation dirs, etc. Maybe I will do this later. 
So I offer a launch script for the built binary file (browser's named basilisk, as non-official Palemoon version) - basilisk_launch.sh
It contains optional strings. Check the file and edit it for your PC settings.
You may edit the template to rename olders to basilisk and register the paths, if you want. I didn't have time for this, although it should be done.

Building:

As usual Void package. Copy the palemoon or palemoon-debug folder in void-packages/srcpkgs/palemoon and run ./xbps-src for palemoon package.

Important note: Palemoon source is quite fat itself. Template contains the necessary path, but it takes long time to download and direct download usually fails. 
So it's better to download it and put source in build tree, as:
void-packages/hostdir/sources/palemoon-28.8.3/PM28.8.3_Release.tar.gz

How to download it: 
wget -T20 -c --show-progress https://github.com/MoonchildProductions/UXP/archive/PM28.8.3_Release.tar.gz
Options -T20 and -c warrant successfull download from githhub.

Warrning! Building Palemoon takes a lot of external sources downloads and build takes around 10Gb of space on disk. On i7 with HDD it took a couple of hours. 
Good luck.

Sincerely,  Iron Bug
