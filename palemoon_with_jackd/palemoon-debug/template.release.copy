# Template file for 'palemoon'
#
# THIS PKG MUST BE SYNCHRONIZED WITH "srcpkgs/palemoon-i18n".
#
pkgname=palemoon
version=28.8.3
revision=2
wrksrc="UXP-PM28.8.3_Release"
short_desc="Palemoon web browser"
#todo: change maintainer and check license, distfiles link, wrksrc(?)
maintainer="Eivind Uggedal <eivind@uggedal.com>"
homepage="https://www.palemoon.org"
license="MPL-2.0, GPL-2.0-or-later, LGPL-2.1-or-later"
distfiles="https://github.com/MoonchildProductions/UXP/archive/PM28.8.3_Release.tar.gz
 https://aur.archlinux.org/cgit/aur.git/plain/ffmpeg4.patch.gz?h=firefox-esr52>ffmpeg4.patch.gz"
checksum="488c1065a835cdd05c8e85ea63633b473c95a66f7b2f4909f092d3e04edb1ccd
 2a285e0c31968e3fe9b65a585838b46f9342ff0a369e786a729b4c3886617034"


lib32disabled=yes
hostmakedepends="autoconf213 unzip zip pkg-config perl python3 yasm tar"
# $(vopt_if rust cargo)"
makedepends="
 libatomic-devel nss-devel gtk+-devel
 sqlite-devel libevent-devel libnotify-devel libvpx-devel
 hunspell-devel libXcomposite-devel libSM-devel libXt-devel icu-devel
 $(vopt_if jack jack-devel)
 $(vopt_if alsa alsa-lib-devel) $(vopt_if dbus dbus-glib-devel)
 $(vopt_if gtk3 gtk+3-devel) 
 $(vopt_if startup_notification startup-notification-devel)
 $(vopt_if xscreensaver libXScrnSaver-devel)"
depends="nss>=3.27 desktop-file-utils hicolor-icon-theme icu>=63.0"
conflicts="firefox>=0"

build_options="alsa dbus gtk3 jack startup_notification xscreensaver"
build_options_default="alsa dbus gtk3 jack startup_notification xscreensaver"
#desc_option_rust="Build rust components"

#case "$XBPS_TARGET_MACHINE" in
#	i686*|x86_64*) build_options_default+=" rust";;
#esac

post_extract() {
	# commented. applied manually becaue of build errors in this patch applying
	#patch -Np0 -i ./ffmpeg4.patch

	case "$XBPS_TARGET_MACHINE" in
	*-musl)
		# fix musl rust triplet
		#sed -i "s/\(x86_64-unknown-linux\)-gnu/\1-musl/" build/moz.configure/rust.configure
		#cp "${FILESDIR}/stab.h" toolkit/crashreporter/google-breakpad/src/
		;;
	esac

	# Google API key (see http://www.chromium.org/developers/how-tos/api-keys)
	# Note: This is for Void Linux use ONLY.
	echo -n "AIzaSyCIFdBA7eQP43R6kXRwTq7j6Mvj1ITze90" > $wrksrc/google-api-key

	# Mozilla API keys (see https://location.services.mozilla.com/api)
	# Note: This is for Void Linux use ONLY.
	echo -n "cd894504-7a2a-4263-abff-ff73ee89ffca" > $wrksrc/mozilla-api-key
}
do_build() {
	cp "${FILESDIR}/mozconfig" "${wrksrc}/.mozconfig"

	case "$XBPS_TARGET_MACHINE" in
	*-musl)
		echo "ac_add_options --disable-jemalloc" >>.mozconfig
		echo "ac_add_options --enable-gold=no" >>.mozconfig
		;;
	arm*|aarch64*)
		echo "ac_add_options --enable-gold=no" >>.mozconfig
		;;
	esac

	if [ "$CROSS_BUILD" ]; then
		export HOST_CFLAGS="${XBPS_CFLAGS}"
		export HOST_CXXFLAGS="${XBPS_CXXFLAGS}"
		export ac_cv_sqlite_secure_delete=yes \
			ac_cv_sqlite_threadsafe=yes \
			ac_cv_sqlite_enable_fts3=yes \
			ac_cv_sqlite_dbstat_vtab=yes \
			ac_cv_sqlite_enable_unlock_notify=yes \
			ac_cv_prog_hostcxx_works=1
		echo "ac_add_options --target=$XBPS_CROSS_TRIPLET" >>.mozconfig
	fi

	# Append CFLAGS and CXXFLAGS to set work around code which gcc6 would
	# otherwise regard as out-of-specification and allow it to produce a
	# working program.
	export CFLAGS+=" -fno-delete-null-pointer-checks -fno-lifetime-dse -fno-schedule-insns2"
	export CXXFLAGS+=" -fno-delete-null-pointer-checks -fno-lifetime-dse -fno-schedule-insns2"

	export LDFLAGS+=" -Wl,-rpath=/usr/lib/palemoon"

	if [ "$SOURCE_DATE_EPOCH" ]; then
		export MOZ_BUILD_DATE=$(date --date "@$SOURCE_DATE_EPOCH" "+%Y%m%d%H%M%S")
	fi

	export MOZ_MAKE_FLAGS="${makejobs}"

	cat <<! >>.mozconfig
ac_add_options --with-google-api-keyfile="${wrksrc}/google-api-key"
ac_add_options --with-mozilla-api-keyfile="${wrksrc}/mozilla-api-key"
ac_add_options --enable-default-toolkit=cairo-gtk$(vopt_if gtk3 '3' '2')
ac_add_options $(vopt_enable alsa)
ac_add_options $(vopt_enable dbus)
ac_add_options $(vopt_enable dbus necko-wifi)
ac_add_options $(vopt_enable jack)
#ac_add_options $(vopt_enable pulseaudio)
#ac_add_options $(vopt_enable rust)
ac_add_options $(vopt_enable startup_notification startup-notification)
!

	make -f client.mk build
}
do_install() {
	make -f client.mk DESTDIR="$DESTDIR" install

	vinstall ${FILESDIR}/vendor.js 644 usr/lib/palemoon/browser/defaults/preferences
	vinstall ${FILESDIR}/palemoon.desktop 644 usr/share/applications

	for i in 16x16 22x22 24x24 32x32 48x48 256x256; do
		vinstall ${wrksrc}/application/palemoon/branding/official/default${i%x*}.png 644 \
			usr/share/icons/hicolor/${i}/apps palemoon.png
	done

	# Use system-provided dictionaries
	rm -rf ${DESTDIR}/usr/lib/palemoon/{dictionaries,hyphenation}
	ln -s /usr/share/hunspell ${DESTDIR}/usr/lib/palemoon/dictionaries
	ln -s /usr/share/hyphen ${DESTDIR}/usr/lib/palemoon/hyphenation

	# We don't want the development stuff
	rm -rf ${DESTDIR}/usr/{include,lib/palemoon-devel,share/idl}

	# https://bugzilla.mozilla.org/show_bug.cgi?id=658850
	ln -sf palemoon ${DESTDIR}/usr/lib/palemoon/palemoon-bin
}
