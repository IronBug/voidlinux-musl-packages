#!/bin/bash
export GTK_THEME=Adwaita:dark
# set your LIBVA driver here, if needed
LIBVA_DRIVER_NAME=i965
export LIBVA_DRIVER_NAME

mkdir -p $HOME/.cache/moonchild\ productions/basilisk
export LD_LIBRARY_PATH=/usr/lib/basilisk 
firejail basilisk > /dev/null 2>&1 
#--disk-cache-dir
#--disk-cache-size 
# add necessary params, if needed
